from django.contrib import admin
from .models import Post, Comment
#esRegister your models here.
#admin.site.register(Post)

@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display= ('title','status','created','publish','author')
    prepopulated_fields={'slug':('title',)}
    search_fields=('title','body')
    ordering=('author','status','publish')
    list_filter=('author','status','publish')

@admin.register(Comment)
class comments(admin.ModelAdmin):
    list_display= ['username','email','created']