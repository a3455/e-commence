from  django import forms
from django.forms.widgets import TextInput
from  .models import Comment


class  CommentForm(forms.ModelForm):
    username = forms.CharField(max_length=100,widget=TextInput(attrs={'class':'form-control'}))
    email = forms.EmailField(widget=TextInput(attrs={'class':'form-control'}))
    body = forms.CharField(max_length=80,widget=TextInput(attrs={'class':'form-control','rows':10}))
    class  Meta:
        model=Comment 
        fields=['username','email','body']